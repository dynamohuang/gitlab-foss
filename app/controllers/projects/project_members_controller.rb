# frozen_string_literal: true

class Projects::ProjectMembersController < Projects::ApplicationController
  include MembershipActions
  include MembersPresentation
  include SortingHelper

  # Authorize
  before_action :authorize_admin_project_member!, except: [:index, :leave, :request_access]

  feature_category :projects
  urgency :low

  def index
    @sort = params[:sort].presence || sort_value_name

    @group_links = @project.project_group_links
    @group_links = @group_links.search(params[:search_groups]) if params[:search_groups].present?

    if can?(current_user, :admin_project_member, @project)
      @invited_members = present_members(invited_members)
      @requesters = present_members(AccessRequestsFinder.new(@project).execute(current_user))
    end

    @project_members = present_members(non_invited_members.page(params[:page]))
  end



  def destroy
    project_members =  ProjectMember.find(params[:id])
    username = User.find(project_members.user_id).username
    json_event = Hash.new
    json_event["subject"] = "project role"
    json_event["type"] = "delete"
    json_event["inviter"] = current_user.username
    json_event["invitee"] = username
    json_event['project'] = @project.full_path
    Gitlab::AppLogger.info("[json even] #{JSON.generate(json_event)}")
    super
  end


  def update
    body = JSON.parse(request.body.read)
    project_members =  ProjectMember.find(params[:id])
    username = User.find(project_members.user_id).username

    json_event = Hash.new
    json_event["subject"] = "project role"
    json_event["type"] = "update"
    json_event["inviter"] = current_user.username
    json_event["invitee"] = username
    json_event['project'] = @project.full_path

    if body['project_member']['access_level'] != nil
      json_event["update_where"] = 'access_level'
      json_event["origin_value"] =project_members.access_level
      json_event["new_value"] = body['project_member']['access_level']
    else
      json_event["update_where"] = 'expires_at'
      json_event["origin_value"] = project_members.expires_at
      json_event["new_value"] = body['project_member']['expires_at']

    end
    Gitlab::AppLogger.info("[json even] #{JSON.generate(json_event)}")
    super
  end

  # MembershipActions concern
  alias_method :membershipable, :project

  private

  def members
    @members ||= MembersFinder
      .new(@project, current_user, params: filter_params)
      .execute(include_relations: requested_relations)
  end

  def invited_members
    members.invite.with_invited_user_state
  end

  def non_invited_members
    members.non_invite
  end

  def filter_params
    params.permit(:search).merge(sort: @sort)
  end

  def membershipable_members
    project.members
  end

  def plain_source_type
    'project'
  end

  def source_type
    _("project")
  end

  def members_page_url
    project_project_members_path(project)
  end

  def root_params_key
    :project_member
  end
end

Projects::ProjectMembersController.prepend_mod_with('Projects::ProjectMembersController')
